# Experiments with SVG

- [Container Elements](01-container-elements/README.md)
- [Basic Elements](02-basic-elements/README.md)
- [Descriptive Elements](03-descriptive-elements/README.md)
- [Animation Elements](04-animation-elements/README.md)
- [Links](Links.md)